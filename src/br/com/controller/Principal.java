package br.com.controller;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.com.conexaobanco.ConexaoBanco;
import br.com.view.TelaPrincipal;

public class Principal {
	
	
	TelaPrincipal telaPrincipal = new TelaPrincipal();
	ConexaoBanco conexao = new ConexaoBanco();
	
	public void enviaCadastroCliente(String sql) throws SQLException{
		
		PreparedStatement ps = ConexaoBanco.getConexaoMySQL().prepareStatement(sql);
		ps.execute();
	}
	
    public static void main(String args[]) throws SQLException {
 
   
        System.out.println(ConexaoBanco.getConexaoMySQL());
        
        
        

        
        
    	//Abrindo a tela principal
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	
                new TelaPrincipal().setVisible(true);
                
            }
        });
    }
}
