package br.com.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.conexaobanco.ConexaoBanco;

public class GenerateCSV {
		public String sFileName;

	   	public  void generateCsvFile() throws SQLException{
		String caminho = System.getProperty("user.home");

		//generateCsvFile(caminho + "\\Desktop\\test.csv"); 
		sFileName = (caminho + "\\Desktop\\banco.csv"); 
		try
	    {
	        FileWriter writer = new FileWriter(sFileName);
	        writer.append("EMPRESA;ID_CLIENTE;CLI_CPF;CLI_EMAIL;CLI_ATIVIDADE;CLI_TAXA_DEBITO;CLI_TAXA_CORRENTE " );
	        writer.append("\n");
	        String sql = "Select * from Cliente";
	        PreparedStatement ps = ConexaoBanco.getConexaoMySQL().prepareStatement(sql);
	        ResultSet rs = ps.executeQuery();
	        
	        while (rs.next()) {
	        	
	            String bool = "";
	            String empresa ;
	            if(rs.getString("cli_empresa_atual").equals("1")){
	            	empresa = "PAGSEGURO";
	            }else if(rs.getString("cli_empresa_atual").equals("2")){
	            	empresa = "SUMUP";
	            }else if(rs.getString("cli_empresa_atual").equals("3")){
	            	empresa = "STELO";
	            }else if(rs.getString("cli_empresa_atual").equals("4")){
	            	empresa = "CIELO";
	            }else if(rs.getString("cli_empresa_atual").equals("5")){
	            	empresa = "IZZETLE";
	            }else if(rs.getString("cli_empresa_atual").equals("6")){
	            	empresa = "MERCADO PAGO";
	            }else{
	            	empresa = "erro";
	            
	            }
	            
	            writer.append(empresa);
	            writer.append(";");
	            writer.append(rs.getString("id_cliente"));
	            writer.append(";");
	            writer.append(rs.getString("cli_cpf"));
	            writer.append(";");
	            writer.append(rs.getString("cli_email"));
	            writer.append(";");
	            writer.append(rs.getString("cli_atividade"));
	            writer.append(";");
	            writer.append(rs.getString("cli_taxa_debito"));
	            writer.append(";");
	            writer.append(rs.getString("cli_taxa_corrente"));
	            writer.append("\n");
	            
	            
	        }

	        //generate whatever data you want

	        writer.flush();
	        writer.close();
	    }
	    catch(IOException e)
	    {
	         e.printStackTrace();
	    } 
	    }
	}