//Nome do nosso pacote //                
 
package br.com.conexaobanco;                   
 
  
 
//Classes necess�rias para uso de Banco de dados //
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

  
 
//In�cio da classe de conex�o//
 


public class ConexaoBanco {
	
	 protected static String queryClientes;
	
     public static String status = "N�o conectou...";
 
//M�todo Construtor da Classe//
 
 public ConexaoBanco() {
 
 }
 
  
 
 
 
 
//M�todo de Conex�o//
 
public  static java.sql.Connection getConexaoMySQL() {
 
        Connection connection = null;          //atributo do tipo Connection
        
        
 
try {
 
// Carregando o JDBC Driver padr�o
 
String driverName = "com.mysql.cj.jdbc.Driver";                        

Class.forName(driverName);
 
  
 
// Configurando a nossa conex�o com um banco de dados//
 
            String serverName = "localhost";    //caminho do servidor do BD
 
            String mydatabase ="sielo";        //nome do seu banco de dados
 
            String username = "root";        //nome de um usu�rio de seu BD      
            
            String password = "root";      //sua senha de acesso

            String correcaoFusoHorario = "?useSSL=false&useTimezone=true&serverTimezone=UTC";

            String url = "jdbc:mysql://" + serverName + ":3306"+ "/" + mydatabase + correcaoFusoHorario;
            
 
 

            
            connection = DriverManager.getConnection(url,username,password);
            
            Statement stmt = connection.createStatement();
	        
            
            //Testa sua //  
 
            if (connection != null) {
                System.out.println("conectado");
                status = ("STATUS--->Conectado com sucesso!");
 
            } else {
 
                status = ("STATUS--->N�o foi possivel realizar conex�o");
 
            }
 
  
 
            return connection;
 
  
 
        } catch (ClassNotFoundException e) {  //Driver n�o encontrado
 
  
 
            System.out.println("O driver expecificado nao foi encontrado." + e);
 
            return null;
 
        } catch (SQLException e) {
 
//N�o conseguindo se conectar ao banco
 
            System.out.println("Nao foi possivel conectar ao Banco de Dados.");
 
            return null;
 
        }
 
  
 
    }	
 
  	
 
    //M�todo que retorna o status da sua conex�o//
 
    public static String statusConection() {
 
        return status;
 
    }
 
   
    String sql = "insert into cliente";
   //M�todo que fecha sua conex�o//
   
 
    public static boolean FecharConexao() {
 
        try {
 
            ConexaoBanco.getConexaoMySQL().close();
 
            return true;
 
        } catch (SQLException e) {
        	System.out.println("erro.." + e);
            return false;
 
        }
 
  
 
    }
 
 
 
   //M�todo que reinicia sua conex�o//
 
    public static java.sql.Connection ReiniciarConexao() {
 
        FecharConexao();
 
  
        
        return ConexaoBanco.getConexaoMySQL();
 
    }
    


    
 
}